gitlab page https://gitlab.com/userAA/twitter-clone-romanets-mongodb-gitlab.git
gitlab twitter-clone-romanets-mongodb-gitlab

project twitter-clone-romanets-mongodb
технологии, используемые на фронтенде:
    @hookform/resolvers,
    @material-ui/core,
    @material-ui/icons,
    @material-ui/lab,
    @types/classnames,
    @types/node,
    @types/react,
    @types/react-dom,
    @types/react-redux,
    @types/react-router-dom,
    @types/yup,
    axios,
    classnames,
    date-fns,
    identity-obj-proxy,
    medium-zoom,
    react,
    react-dom,
    react-hook-form,
    react-redux,
    react-router-dom,
    react-scripts,
    redux,
    redux-saga,
    sass,
    sass-loader,
    typescript,
    yup;

технологии, используемые на бекэнде:
    @material-ui/lab,
    @types/body-parser,
    @types/express,
    @types/jsonwebtoken,
    @types/mongoose,
    @types/multer,
    @types/passport,
    @types/passport-jwt,
    @types/passport-local,
    body-parser,
    cloudinary,
    crypto-js,
    dotenv,
    express,
    express-validator,
    jsonwebtoken,
    mongoose,
    multer,
    nodemon,
    passport,
    passport-jwt,
    passport-local,
    ts-node,
    typescript;

Авторизованный пользователь создает твиты, которые можно удалять или добавлять. 
Твиты состоят из текста или изображений. Также основная страница представляет полный список твитов 
для всех пользователей, в том числе и неавторизованных. Можно получить всю информацию о твите просто нажав 
на него.

